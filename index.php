<?php // include 'inc/initconfig.php';                                           ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Slant - One Page Template">
        <meta name="author" content="Katon - katonpress.net">
        <link rel="shortcut icon" href="img/icon/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/icon/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="img/icon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="img/icon/apple-touch-icon-114x114.png" />

        <title> Houston Nightlife Events </title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <!-- Custom styles for this template -->
        <link rel="stylesheet" type="text/css" href="css/style.css">



        <!-- Main JS --> 
        <script src="js/lib/jquery.min.js"></script>
        <script type="text/javascript" src="js/lib/modernizr.custom.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <style>	.animated {	opacity: 1;	} </style>
          <script src="js/lib/html5shiv.min.js"></script>
          <script src="js/lib/respond.min.js"></script>
        <![endif]-->
        <script>
            $(document).ready(function () {

                $('.item a').click(function (e) {
                    e.preventDefault();
                    var url = $(this).attr('href');
                    window.open(url, '_blank');
                });

            });
        </script>


    </head>

    <body data-spy="scroll" data-offset="0" data-target="#navbar-main">

        <header class="clearfix">

            <div id="navbar-main">
                <!-- Fixed navbar -->
                <div class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="fa fa-bars" style="font-size:30px; color:#FFF; "></span>
                            </button>
<!--                            <a class="navbar-brand " href="#home"><span class="fa fa-paper-plane-o blue-text" style="font-size:25px;"></span> SLANT </a>-->
                            <a class="navbar-brand " href="#home" style="padding: 0;background-color: transparent"><img src="img/logo.png"></a>
                        </div>
                        <div class="navbar-collapse collapse">


                            <ul class="nav navbar-nav ">
                                <li> <a class="active" data-scroll-nav="0" href="#" >Home</a></li>
                                <li> <a data-scroll-nav="1" href="#" > About</a></li>
                                <li> <a data-scroll-nav="2" href="#" > Services</a></li>
                                <li> <a data-scroll-nav="3" href="#" > Venues</a></li>
                                <li> <a data-scroll-nav="4" href="#" > Team</a></li>
                                <li> <a data-scroll-nav="5" href="#" > HOT SPOTS</a></li>
                                <li> <a data-scroll-nav="6" href="#" > Contact</a></li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </div>



            <!-- ==== HOME HEADERWRAP ==== -->
            <section  data-scroll-index="0" id="home">


                <div class="skewSlider main-slide" id="homeslider">
                    <ul>
                        <li><div class="skw-content" data-caption="Bottle Services" data-bg="img/slider/slide1.jpg" ></div></li>
                        <li><div class="skw-content" data-caption="Event Tickets " data-bg="img/slider/slide2.jpg"></div></li>
                        <li><div class="skw-content" data-caption="Party Planing" data-bg="img/slider/slide3.jpg"></div></li>
                    </ul>
                </div><!-- /homeslider -->
            </section>

        </header>	   


        <!-- ==== About us ==== -->
        <section  data-scroll-index="1" id="about" class="extra-padding" >
            <div class="row">
                <div class="container" >

                    <h2 data-animated="fadeIn" class="centered">All You Need Right Here.</h2>

                    <div data-animated="fadeIn" class="col-lg-offset-2 col-lg-8">

                        <p class="centered" >Discover exclusive deals from local bars, clubs, and restaurants that can only be provided to our select members.</p>

                    </div><!-- col-lg -->
                    <br><br>

                    <div data-animated="fadeInLeft" class="col-lg-4 col-md-4 col-sm-12 ">
                        <div class="shape">
                            <a href="#" class="overlay hexagon"></a>
                            <div class="details">
                                <p>We get exclusive deal for bottle reservations</p>		
                                <a href="http://www.eventbrite.com/e/your-hne-event-planning-tickets-17506394096?aff=es2" class="button" target="_blank">VIEW</a>		
                            </div>
                            <div class="bg"></div>	<div class="base">
                                <img src="img/about/01.jpg" alt="" />
                            </div>
                        </div>	
                        <h2 class="centered">SAVE MONEY</h2>
                    </div><!-- col-lg-4 col-md-4 col-sm-12 -->

                    <div data-animated="fadeIn"  class="col-lg-4 col-md-4 col-sm-12 ">
                        <div class="shape">
                            <a href="#" class="overlay hexagon"></a>
                            <div class="details">
                                <p>Get exclusive access to events</p>		
                                <a href="http://www.eventbrite.com/e/your-hne-event-planning-tickets-17506394096?aff=es2" class="button" target="_blank">VIEW</a>		
                            </div>
                            <div class="bg"></div>	<div class="base">
                                <img src="img/about/02.jpg" alt="" />
                            </div>
                        </div>	
                        <h2 class="centered">AVOID LINES & BE VIP</h2>
                    </div><!-- col-lg-4 col-md-4 col-sm-12 -->	

                    <div data-animated="fadeInRight" class="col-lg-4 col-md-4 col-sm-12 ">
                        <div  class="shape">
                            <a href="#" class="overlay hexagon"></a>
                            <div class="details">
                                <p>Plan your events with us</p>		
                                <a href="http://www.eventbrite.com/e/your-hne-event-planning-tickets-17506394096?aff=es2" class="button" target="_blank">VIEW</a>		
                            </div>
                            <div class="bg"></div>	<div class="base">
                                <img src="img/about/03.jpg" alt="" />
                            </div>
                        </div>	
                        <h2 class="centered">PARTY HOW YOU WANT IT</h2>
                    </div><!-- col-lg-4 col-md-4 col-sm-12 -->	

                    <div class="clearfix"></div>
                </div><!-- container -->
            </div><!-- row -->


            <br />
            <br />


            <!-- ==== About Section 2 ==== -->
            <div class="row" >



                <div class="container white extra-margin"  >
                    <hr>

                    <div  class="col-lg-6 col-md-6 extra-margin-top" >
                        <br>
                        <h1 data-animated="fadeIn" >ABOUT OUR SERVICE</h1>
                        <p data-animated="fadeIn" >As a HNE Member, you get access to the latest city events at the best bars, clubs, and restaurants in town. Whether deals or specials on a venues services or food/drinks, or availability of exclusive VIP access and reserved venue seating, we offer our subscribers the most update, personal service to experiencing the best of the Houston nightlife. If you have an upcoming event, whether its a birthday or even an organization event, call us to find the venue for you a negotiable deals</p>

                        <div data-animated="fadeIn" >
                            <a class="open-popup btn btn-slant  fa fa-play-circle-o" href="#aboutvid">Watch Movie</a>
                        </div>

                        <div id="aboutvid" class="transparent-popup mfp-hide ">
                            <div class="col-md-12 fitvideo">
<!--					 <iframe width="560" height="315" src="https://www.youtube.com/embed/3G-t72JjRf0" ></iframe>-->
                            </div>

                            <div class="clearfix"></div>
                        </div>




                    </div>
                    <div data-animated="fadeInRightBig" class="col-lg-6 col-md-6 centered extra-padding-top" >
                        <img src="img/about/iphone.png" alt="phone" />
                    </div><!-- col-lg-6 col-md-6 -->

                </div><!-- container -->

            </div><!-- row -->

        </section><!-- section about end-->


        <section  data-scroll-index="2" id="services"  >
            <!-- ==== SECTION DIVIDER1 -->
            <div class="section-divider  services parallax-bg fixed">
                <div class="parallax">
                    <div class="triangle-down"></div>

                    <div class="container extra-margin">
                        <div data-animated="fadeInUpBig" class="col-lg-5 col-lg-offset-7 col-md-offset-7 col-sm-offset-3">


                            <h1>DISCOVER FOR YOURSELF WHAT HOUSTON HAS TO OFFER </h1>

                            <p>Our events are as BIG as our city! We are culturally diverse, and have something for everyone's' taste</p>

                        </div><!-- col-lg-6 col-md-6 -->
                    </div><!-- container -->
                </div>
            </div><!-- divider -->




            <!-- ==== blue-bgWRAP ==== -->

            <div class="row blue-bg extra-padding">
                <div class="container">


                    <div data-animated="fadeIn" class="col-lg-6 col-md-6 ">

                        <h2><i class="fa fa-empire"></i> PRIVATE EVENTS </h2>
                        <p>We plan and customize your event to the specifications you request. If you are unsure of what you want, no worries, as we can help you organize the best plan to satisfy you and your guests.</p>
                    </div><!-- col-lg-4 col-md-4 col-sm-12 -->	

                    <div data-animated="fadeIn" class="col-lg-6 col-md-6 ">

                        <h2><i class="fa fa-codepen"></i> PUBLIC EVENTS</h2>
                        <p>Interested in holding a public event of your own? We constantly partner with events sponsored around the city, as we are networked with the biggest and most popular venues in Houston. We can be the marketing solution to getting you your desire guest-turnout for your event.</p>
                    </div><!-- col-lg-4 col-md-4 col-sm-12 -->	
                </div><!-- container -->
            </div><!-- row -->

        </section><!-- Service section -->



        <!-- ==== PORTFOLIO ==== -->
        <section  data-scroll-index="3" id="portfolio" >
            <div class="row">

                <br>
                <br>
                <div class="row">
                    <h2 class="centered">ALL YOU NEED RIGHT HERE</h2>

                    <br>
                    <div class="col-lg-offset-2 col-lg-8">
                        <p class="centered">Below are the bars, nightclubs, and restaurants participating in the Houston Nightlife Network. If there is a desired place you don't see us offering, let us know and we will possible make the connection. We seek a cultural-fit, of a fun and welcoming atmosphere, in which all of our current partners posses</p>

                    </div><!-- col-lg -->
                </div><!-- row -->




                <div class="clearfix"></div>

                <!-- Portfolio Filter Start -->		
                <section id="options" class="clearfix">
                    <div class="container">
                        <ul id="filters" class="option-set portfolioFilter clearfix" data-option-key="filter">
                            <li><a href="#filter" data-filter="*" class="btn btn-slant btn-bordered active">show all </a></li>
                            <li><a href="#filter" data-filter=".photography" class="btn btn-slant btn-bordered ">photography </a></li>
                            <li><a href="#filter" data-filter=".video" class="btn btn-slant btn-bordered ">video</a></li>
                        </ul>
                    </div>
                </section><!-- #options -->

                <!-- Portfolio Grid Start -->	
                <section  class="grid-wrap">
                    <ul class="grid swipe-right" id="grid">





                        <li class="item photography"><a class="open-popup" href="http://clubaurahouston.com/" target="_blank">
                                <img src="img/portfolio/aura2.jpg" alt="item">
                                <h3>AURA</h3></a>
                        </li>

                        <li class="item photography"><a class="open-popup" href="http://mrpeeples.com/" target="_blank">
                                <img src="img/portfolio/mrpeeples.jpg" alt="item">
                                <h3>Mr.Peeples</h3></a>
                        </li>

                        <li class="item photography"><a class="open-popup" href="http://thedegaulle.com/" target="_blank">
                                <img src="img/portfolio/degaulle.jpg" alt="item">
                                <h3>De Gaulle </h3></a>
                        </li>
                        <li class="item photography"><a class="open-popup" href="http://galwayhoustonpub.com/" target="_blank">
                                <img src="img/portfolio/galway.jpg" alt="item">
                                <h3>Galway Hooker</h3></a>
                        </li>
                        <li class="item photography"><a class="open-popup" href="http://giuliettasociale.com/" target="_blank">
                                <img src="img/portfolio/giulietta_patio.jpg" alt="item">
                                <h3>Giulietta Sociale</h3></a>
                        </li>

                        <li class="item photography"><a class="open-popup" href="http://hugheshangar.com/" target="_blank">
                                <img src="img/portfolio/hughes_hangar.jpg" alt="item">
                                <h3>Hughes Hangar</h3></a>
                        </li>
                        <!--                        <li class="item photography"><a class="open-popup" href="http://lumenloungehouston.com/" target="_blank">
                                                        <img src="img/portfolio/lumen.jpg" alt="item">
                                                        <h3>Lumen Lounge</h3></a>
                                                </li>-->
                        <li class="item photography"><a class="open-popup" href="http://lumenloungehouston.com/" target="_blank">
                                <img src="img/portfolio/lumen.jpg" alt="item">
                                <h3>Lumen Lounge</h3></a>
                        </li>
                        <li class="item photography"><a class="open-popup" href="http://proofrooftoplounge.com/" target="_blank">
                                <img src="img/portfolio/proofbar.jpg" alt="item">
                                <h3>Proof Rooftop Bar</h3></a>
                        </li>
                        <li class="item photography"><a class="open-popup" href="http://reddoormidtown.com/" target="_blank">
                                <img src="img/portfolio/reddoor.jpg" alt="item">
                                <h3>Red Door Midtown</h3></a>
                        </li>
                        <li class="item photography"><a class="open-popup" href="http://rosemonthouston.com/" target="_blank">
                                <img src="img/portfolio/rosemont.jpg" alt="item">
                                <h3>Rosemont Social Club</h3></a>
                        </li>
                        <li class="item photography"><a class="open-popup" href="http://soleterrace.com/" target="_blank">
                                <img src="img/portfolio/Sole_Terrace_Updated.jpg" alt="item">
                                <h3>Solé Terrace</h3></a>
                        </li>
                    </ul>
                </section>
                <!-- Portfolio Grid End -->	




            </div><!-- row -->



            <!-- ==== Testimonial -->
            <div class="section-divider white-text testimonial parallax-bg">
                <div class="color-overlay"></div>
                <div class="parallax">

                    <div class="container extra-padding" >

                        <p class="centered" ><i class="fa fa-quote-left"></i></p>

                        <div  class="clients owl-carousel owl-theme centered" >

                            <div class="item">
                                <p>We needed to get our Saturday nights going. Houston Nightlife Events helped us reach where we are today, and if you check us out Saturdays you'll see how amazing it has been</p>
                                <h3>Megan Gwillim  

                                </h3> <span>GIULIETTA SOCIALE</span>

                            </div>

                            <div class="item">
                                <p>Our Wednesday steak nights wouldn't be the same without Houston Nightlife Events, as we love our UH students who join us for watching sports events on our 42 TVs.</p>
                                <h3>Ali Shahid </h3> <span>GALWAY HOOKER</span>

                            </div>
                            <div class="item">
                                <p>Houston Nightlife Events has brought us some much business in the form of private events. We couldn't thank them enough.

                                </p>
                                <h3>Heather Flower</h3> <span>HUGHES HANGAR</span>

                            </div>


                        </div><!-- owl-->
                    </div><!--container-->



                </div><!--parallax-->	
            </div><!-- testimonial divider -->

        </section> <!-- Portfolio Section -->



        <!-- ==== TEAM MEMBERS ==== -->
        <section  data-scroll-index="4" id="team" >
            <div class="container" style="padding:30px 0;">

                <div class="row white centered">
                    <h1 class="centered">MEET OUR AWESOME TEAM</h1>
                    <div class="col-lg-offset-2 col-lg-8">
                        <p>Our company is only possible because we employ the very best, sociable individuals who thoroughly cater to our members' needs. Know for yourself when you speak to them over the phone to book or plan a reservation, or event when our representatives are hosting the sponsored events
                        </p>


                    </div><!-- col-lg -->	
                </div><!-- row -->
            </div><!-- container -->



            <div class="cont"> 
                <div class="skewSlider slide1 team" id="teamslider">
                    <ul>
                        <li><div class="skw-content slide">
                                <img src="img/team/01.png" alt="person" />
                                <div class="text">
                                    <h3>Sean Jaehne</h3>
                                    <h4>Founder</h4>
                                    <p>Sean is our founder and main event planner for the biggest events we hold.</p> 

                                    <ul class="social-network social-circle social-small">
                                        <li><a href="https://www.facebook.com/rbhoustonevents" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/HNENetwork" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="https://plus.google.com/u/0/116678322202830390445/posts" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>	

                                </div>
                            </div></li>
                        <li><div class="skw-content slide">
                                <img src="img/team/02.png" alt="person" />
                                <div class="text">
                                    <h3>Bruce Zelaya</h3>
                                    <h4>Photographer / Media Production</h4>
                                    <p>Photographer / Media Production
                                        Bruce is our video and photo guy that does fantastic work. He also works freelance if you're needing such services</p> 

                                    <ul class="social-network social-circle social-small">
                                        <li><a href="https://www.facebook.com/bruce.w.zelaya?fref=ts" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/HNENetwork" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="https://plus.google.com/u/0/110749833598943206293/posts" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>	

                                </div>
                            </div></li>
                        <li><div class="skw-content slide">
                                <img src="img/team/03.png" alt="person" />
                                <div class="text">
                                    <h3>Priscilla Schonacher</h3>
                                    <h4>Go-Go Dancer</h4>
                                    <p>Priscilla is a skilled dance artist that our venues hire for special events and that you can hire for your private events as well.</p> 

                                    <ul class="social-network social-circle social-small">
                                        <li><a href="https://www.facebook.com/pschonacher?fref=ts" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/HNENetwork" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="https://plus.google.com/u/0/107163600347847719366/posts" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>	

                                </div>
                            </div></li>






                        <li><div class="skw-content slide">
                                <img src="img/team/03.png" alt="person" />
                                <div class="text">
                                    <h3>Aziza GiGi McWhirte</h3>
                                    <h4> Promoter </h4>
                                    <p></p> 
                                    <ul class="social-network social-circle social-small">
                                        <li><a href="https://www.facebook.com/gmcwhirter24?fref=ts" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/HNENetwork" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="mailto:aziza.houstonnightlife@gmail.com" class="icoGoogle" title="Google +"><i class="fa fa-mail-reply"></i></a></li>
                                    </ul>	
                                </div>
                            </div></li>

                    </ul>
                </div>
            </div> 




            <!-- ==== blue-bgWRAP ==== -->

            <div class="row dark dark-text" style="padding:20px 0;">
                <div class="container">


                    <div class="col-lg-4 col-md-4 col-sm-12 centered">


                        <div class="countto"><p class="counter"><span class="icon"><i class="fa fa-bolt "></i></span><b class="timer count" data-to="1333" data-speed="3000"></b> <span class="title">Bottles Served</span></p></div>



                    </div><!-- col-lg-4 col-md-4 col-sm-12 -->	

                    <div class="col-lg-4 col-md-4 col-sm-12 centered ">
                        <div class="countto"><p class="counter"><span class="icon"><i class="fa fa-history "></i></span><b class="timer count" data-to="1500" data-speed="3000"></b> <span class="title">Ticket Sold </span></p></div>


                    </div><!-- col-lg-4 col-md-4 col-sm-12 -->	

                    <div class="col-lg-4 col-md-4 col-sm-12 centered">
                        <div class="countto"><p class="counter"><span class="icon"><i class="fa fa-magnet"></i></span><b class="timer count" data-to="2423" data-speed="3000"></b> <span class="title">Parties Planed</span></p></div>


                    </div><!-- col-lg-4 col-md-4 col-sm-12 -->	

                </div><!-- container -->
            </div><!-- row -->

        </section> <!-- Team Section -->



        <!--Video DIVIDER2 -->

        <a class="player" id="player" data-property="{
           videoURL: 'https://www.youtube.com/watch?v=Tnen2CQUxUI',
           containment:'#videobg',
           autoPlay: true,
           optimizeDisplay: true,
           showControls: true,
           startAt: 0,
           opacity: 1,
           ratio: '4/3', 
           addRaster: false }">
        </a>


        <div id="videobg" class="section-divider dark-text videobg">
            <div class="pattern-overlay"></div>
            <div class="triangle-right"></div>

            <div class="container">
                <div class="col-md-5" style="padding:140px 0;">


                </div>
                <div class="col-md-2" style="padding:140px 0;"></div>
                <div class="col-md-5" style="padding:140px 0;text-align: right">
                    <h1>Top Clubs in the World</h1>
                    <p>To develop a deeper and more meaningful connection with consumers, we believe design must invite them to take part in the conversation.</p>
                </div>
            </div><!-- container -->
        </div><!-- section -->
        <!-- /video -->


        <!-- ==== BLOG ==== -->

        <section  data-scroll-index="5" id="blog" >
            <div class="container" >

                <div class="row">
                    <br>
                    <h1 class="centered">OUR EVENTS</h1>
                    <h3 class="centered"> See Schedule</h3>
                    <br>
                    <br>
                </div><!-- /row -->

                <div class="row">
                    <div data-animated="fadeInLeft" class="col-lg-4 col-md-4 col-sm-12">
                        <div class="blog-bg">
                            <h2><a href="#">FRIDAY</a></h2>

                            <img class="img" src="img/blog/lumen2.jpg" alt="artical" />

                            <div class="col-lg-12 blog-content">

                                <div class="col-lg-8 col-md-8 col-sm-8">				
                                    <p>Combining cutting edge sound and lights with unparalleled comfort and design, Lumen is the leader in high-end nightlife in Houston. The club's innovative interior is designed so you can see and be seen, showcasing the city's premier clientele in a Vegas inspired fashion. Expect to see top internationally touring house DJs on a weekly basis.</p>
                                    <p><a href="https://www.eventbrite.com/e/lumen-lounge-discounted-reservations-tickets-17506135322" class="more fa  fa-external-link" target="_blank"> Read More</a></p>
                                    <br>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 centered">
                                    <br>
                                    <p><img class="img img-circle" src="img/blog/abdullumen.jpg" alt="author" /></p>
                                    <h4>Abdul Ali</h4>	
                                </div>



                            </div>
                        </div></div><!-- /col -->

                    <div data-animated="fadeInLeft" class="col-lg-4 col-md-4 col-sm-12">
                        <div class="blog-bg">
                            <h2><a href="#">SATURDAY</a></h2>
                            <img class="img" src="img/blog/giulietta.jpg" alt="artical" />



                            <div class="col-lg-12 blog-content">

                                <div class="col-lg-8 col-md-8 col-sm-8">				
                                    <p>An Italian social lounge with live music, and the city's Latin heat every Saturday. Giulietta is arguably the most unique style of club in Houston. </p>
                                    <p><a href="https://www.eventbrite.com/e/giulietta-bottle-reservations-tickets-17148520687" class="more fa  fa-external-link" target="_blank"> Read More</a></p>
                                    <br>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 centered">
                                    <br>
                                    <p><img class="img img-circle" src="img/blog/pro-pic-2.jpg" alt="author" /></p>
                                    <h4>Sean Jaehne</h4>
                                </div>


                            </div>


                        </div></div><!-- /col -->


                    <div data-animated="fadeInLeft" class="col-lg-4 col-md-4 col-sm-12">
                        <div class="blog-bg">
                            <h2><a href="#">Sunday-Fundays</a></h2>

                            <img class="img" src="img/blog/hugheshangar.jpg" alt="artical" />

                            <div class="col-lg-12 blog-content">

                                <div class="col-lg-8 col-md-8 col-sm-8 ">	
                                    <p>Since it's opening in 2011, Hughes Hangar has always boasted the best Sunday-Fundays in Houston. With both patio lounge and indoor dancing, many Houstonians and touring celebrities land onto the fun-strip of Hughes Hangar. </p>
                                    <p><a href="https://www.eventbrite.com/e/hughes-hangar-sunday-fundays-tickets-17506598708" class="more fa  fa-external-link" target="_blank"> Read More</a></p>
                                    <br>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 centered">
                                    <br>
                                    <p><img class="img img-circle" src="img/blog/Heatherhugheshangar.jpg" alt="author" /></p>
                                    <h4>Heather Flower</h4>
                                </div>


                            </div>


                        </div></div><!-- /col -->

                    <div class="row centered clearfix " >

                        <a href="https://www.google.com/calendar" class="extra-margin-top btn btn-slant  fa fa-bold" target="_blank">SCHEDULE</a>
                    </div>


                </div><!-- /row -->
                <br>
                <br>
            </div><!-- /container -->

        </section><!-- blog-->

        <!-- ==== SECTION Contact ==== -->
        <section  data-scroll-index="6" id="contact" class="section-divider dark-text contact" >

            <div class="container extra-padding-bottom">


                <div class="row">


                    <div class="col-md-5 contact-info">

                        <h1>WE ARE HOUSTONIANS</h1>
                        <div class="col-md-8">

                            <p>Sean Jaehne</p>
                            <p><i class="fa fa-phone "></i> +1 281-658-9593</p>

                        </div><!--colmd8-->	
                    </div><!--colmd5-->
                    <div class="col-md-7">
                        <h1>RESERVATION REQUEST</h1>

                        <div class="cform" id="contact-form">

                            <form action="contact/contact.php" method="post" role="form" class="contactForm">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Venue Destination" data-rule="maxlen:4" data-msg="Please enter at least 8 chars of subject" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" placeholder="Date, Arrival Time, Number of Guests" data-rule="required" data-msg="Please write something for us"></textarea>
                                    <div class="validation"></div>
                                </div>

                                <button type="submit" class="btn btn-slant pull-left fa fa-long-arrow-right">SEND MESSAGE</button>
                            </form>

                            <div id="sendmessage">
                                Your message has been sent. Thank you!
                            </div>

                        </div>
                    </div>

                </div><!-- /row -->
            </div> 		
        </section><!-- section contact -->

        <div class="footer container centered" >
            <div class="row">
                <br>
                <h1 data-animated="fadeIn" class="centered">THANKS FOR VISITING US</h1>

                <br>
                <img src="img/footer_logo.png">
                <div class="row align-center copyright">
                    <div class="col-sm-12"><p>Powered by <a href="http://phryxus.com/">Phryxus </a></p></div>
                </div>

                <div data-animated="fadeIn" class="row">
                    <div class="col-sm-12 align-center">
                        <ul class="social-network social-circle">

                            <li><a href="https://www.facebook.com/rbhoustonevents" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/HNENetwork" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://plus.google.com/u/0/116678322202830390445/posts" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        </ul>				
                    </div>
                </div>

                <br><br>	

            </div><!-- row -->
        </div><!-- container -->



        <!-- Bootstrap core JavaScript
           ================================================== -->
        <script type="text/javascript" src="js/lib/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lib/jquery.easing.1.3.js"></script>
        <!-- appear --> 
        <script src="js/plugins/appear.js"></script> 
        <!-- Parallax --> 
        <script src="js/plugins/jquery.parallax-1.1.3.js"></script> 
        <!-- The Screw Plugin --> 
        <script type="text/javascript" src="js/plugins/jquery.skewSlider.js"></script>
        <script type="text/javascript" src="js/plugins/plugins.js"></script>
        <!-- Owl --> 
        <script type="text/javascript" src="js/plugins/owl.carousel.min.js"></script>
        <!-- portfolio Grid-->
        <script src="js/plugins/masonry.pkgd.min.js"></script>
        <script src="js/plugins/isotope.pkgd.min.js"></script>
        <script src="js/plugins/classie.js"></script>
        <script src="js/plugins/imagesloaded.pkgd.min.js"></script>
        <script src="js/plugins/colorfinder-1.1.js"></script>
        <script src="js/plugins/gridScrollFx.js"></script>
        <!-- Lightbox -->
        <script src="js/plugins/jquery.magnific-popup.min.js"></script>	
        <!-- Youtube player -->
        <script type="text/javascript" src="js/plugins/jquery.mb.YTPlayer.js"></script>
        <!-- fitvids --> 
        <script src="js/plugins/jquery.fitvids.js"></script> 
        <!-- easypiechart --> 
        <script src="js/plugins/jquery.easypiechart.min.js"></script> 
        <!-- Count --> 
        <script src="js/plugins/count.js"></script> 
        <!-- Scroll --> 
        <script type="text/javascript" src="js/plugins/scrollIt.min.js"></script>
        <!-- Custom Script --> 
        <script src="js/validate.js"></script>
        <script src="js/custom.js"></script> 	
    </body>
</html>
